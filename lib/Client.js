const axios = require('axios')
const Index = require('./Index')
/**
 * This represents a client to intract with API endpoints
 * @constructor
 *
 * @example
 *   const client = new Client(apiKey)
 */
function Client(apiKey) {
  this.apiKey = apiKey
}
/**
 * @description Initialise an index with a unique title. The index will be created if it does not exists.
 *
 * Indexes represent the logical boundaries of your system. They can also map your database entities.
 *
 * **Make sure you don’t use any sensitive or personally identifiable information (PII)** as your index title, including customer names, user IDs, or email addresses. Index titles appear in network requests and should be considered publicly available.
 *
 * @param {string} title - title of an index in a project
 * @return {Index} index - an [index]{@link Index} object
 *
 * @example
 *   const client = new Client(apiKey)
 *   const index = client.initIndex(title)
 */
Client.prototype.initIndex = function(title) {
  return new Index(title)
}

module.exports = Client
