const axios = require('axios')

function Resource(objectId, prefix = '') {
  this.objectId = objectId
  this.prefix = prefix
}
Resource.prototype.update = async function(object, rules, options = {}) {
  try {
    const url = `${this.prefix}resources/${this.objectId}`
    const {searchableAttributes = []} = options
    const data = {
      object,
      searchableAttributes,
      rules
    }
    const config = {
      method: 'put',
      url,
      headers: {
        'Accept': 'application/json'
      },
      data
    };
    const res = await axios(config)
    return res.data
  } catch (err) {
    throw err
  }
}
Resource.prototype.create = async function(object, rules, options = {}) {
  try {
    const url = `${this.prefix}resources`
    const {searchableAttributes = []} = options
    const data = {
      objectId: this.objectId,
      object: object,
      searchableAttributes,
      rules
    }
    const config = {
      method: 'post',
      url,
      headers: {
        'Accept': 'application/json'
      },
      data
    };
    const res = await axios(config)
    return res.data
  } catch (err) {
    throw err
  }
}
Resource.prototype.bulkCreate = async function(objects, options = {}) {
  try {
    const url = `${this.prefix}resources/bulk`
    const {searchableAttributes = []} = options
    const data = {
      objects,
      searchableAttributes
    }
    const config = {
      method: 'post',
      url,
      headers: {
        'Accept': 'application/json'
      },
      data
    };
    const res = await axios(config)
    return res.data
  } catch (err) {
    throw err
  }
}
Resource.prototype.get = async function() {
  try {
    const url = `${this.prefix}resources/${this.objectId}`
    const config = {
      method: 'get',
      url,
      headers: {
        'Accept': 'application/json'
      }
    };
    const res = await axios(config)
    return res.data
  } catch (err) {
    throw err
  }
}
Resource.prototype.delete = async function() {
  try {
    const url = `${this.prefix}resources/${this.objectId}`
    const config = {
      method: 'delete',
      url,
      headers: {
        'Accept': 'application/json'
      }
    };
    const res = await axios(config)
    return res.data
  } catch (err) {
    throw err
  }
}
module.exports = Resource
