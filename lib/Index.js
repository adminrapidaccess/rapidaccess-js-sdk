const axios = require('axios')
const Resource = require('./Resource')
/**
 * This represents an indexes defined by clients in their projects.
 * @constructor
 *
 * @param {string} title - A uniq title of an index
 * @return {Index} index - an [index]{@link Index} object
 *
 * @example
 * const index = new Index('books')
 */
function Index(title) {
  this.title = title
  this.apiPrefix = `/api/v1/indexes/${title}/`
  this.searchPrefix = `/query/v1/indexes/${title}/`
}
/**
 * Update an existing object. `ObjectId` is an uniq identifier and must be exists.
 *
 * @param {string} objectId - The object uniq identifier
 * @param {Object} object - New object to be replaced by the old data
 * @param {Array.<Object>} rules - New rules
 * @param {Objects} [options] - create options
 * @param {Array} [options.searchableAttributes=[]] - object's attributes that get indexed for searching
 *
 * @returns {Object} object - The updated object
 *
 * @throws {NOT_EXISTS} objectId must exist
 *
 * @example
 * await index.updateObject('uid', {title: 'Inside the past', auther: 'Rob Blue'}, rules: [{loc: ['UK', 'AU']}])
 *
 */
Index.prototype.updateObject = async function(objectId, object = {}, rules = [], options) {
  try {
    const resource = new Resource(objectId, this.apiPrefix)
    return await resource.update(object, rules, options)
  } catch(err) {
    throw new Error(err.response.data.error.message)
  }
}
/**
 * Fetch an object.
 *
 * @param {string} objectId - The object uniq identifier
 *
 * @returns {Object} object - The object's data
 *
 * @throws {NOT_EXISTS} objectId must exist
 *
 * @example
 * await index.getObject('uid')
 *
 */
Index.prototype.getObject = async function(objectId) {
  try {
    const resource = new Resource(objectId, this.apiPrefix)
    return await resource.get()
  } catch(err) {
    throw new Error(err.response.data.error.message)
  }
}
/**
 * Deletes an object.
 *
 * @param {string} objectId - The object uniq identifier
 *
 * @returns {string} Deleted object's ID
 *
 * @throws {NOT_EXISTS} objectId must exist
 *
 * @example
 *   await index.deleteObject('object-id')
 *
 */
Index.prototype.deleteObject = async function(objectId) {
  try {
    const resource = new Resource(objectId, this.apiPrefix)
    return await resource.delete()
  } catch(err) {
    throw new Error(err.response.data.error.message)
  }
}
/**
 * Creates an object.
 *
 * @param {string} objectId - The object uniq identifier
 * @param {Object} object - Object
 * @param {Array.<Object>} rules - Access rules for filtering
 * @param {Objects} [options] - create options
 * @param {Array} [options.searchableAttributes=[]] - object's attributes that get indexed for searching
 *
 * @returns {string} Created object's ID
 *
 * @throws {ALREADY_EXISTS} objectId must be unique.
 *
 * @example
 * await index.createObject('object-id', {title: 'Inside the past', authers: ['Rob', 'Jonny']}, [{loc: 'UK'}])
 *
 */
Index.prototype.createObject = async function(objectId, object = {}, rules = [], options) {
  try {
    const resource = new Resource(objectId, this.apiPrefix)
    return await resource.create(object, rules, options)
  } catch(err) {
    throw new Error(err.response.data.error.message)
  }
}
/**
 * Creates objects in bulk. `ObjectId` must be uniq in the index.
 *
 * @param {Array.<Object>} objects - Array of objects
 * @param {string} objects.objectId - Uniq ID of an object
 * @param {Object} objects.object - Object definition
 * @param {Array.<Object>} objects.rules - array of Rules
 * @param {Objects} [options] - create options
 * @param {Array} [options.searchableAttributes=[]] - objects' attributes that get indexed for searching
 *
 * @returns {Array} The objectId of persisted objects
 *
 * @throws {SIZE_EXCEEDED} only accepts objects of size less than or equal to 100.
 * @throws {ALREADY_EXISTS} objectIds must be unique.
 * @throws {MISSING_REQUIRED} objectIds must exist.
 *
 * @example
 * await index.createObjects([{
 *   objectId: '1',
 *   object: {title: 'Inside the past', isbn: '978-3-16-148410-0'},
 *   rules: [loc: 'AU']
 * },
 * {
 *   objectId: '2',
 *   object: {title: 'Trap the midnight hour'},
 *   rules: [loc: 'UK']
 * }], {searchableAttributes: ['title']})
 *
 */
Index.prototype.createObjects = async function(objects, options) {
  try {
    const resource = new Resource(null, this.apiPrefix)
    return await resource.bulkCreate(objects, options)
  } catch(err) {
    throw new Error(err.response.data.error.message)
  }
}
/**
 * Search objects in an index.
 *
 * @param {string} query - Search phrase
 * @param {Array.<Object>} filters - Filter objects
 * @param {Object} options - Search options
 * @param {number} [options.page=0] - Page number of the results
 * @param {number} [options.limit=100] - Number of records in a selected page
 * @returns {Object} Objects with total number of records for the search phrase and the given rules
 *
 * @throws {SIZE_EXCEEDED} max limit per search request is 1000.
 *
 * @example
 * await index.search('past', [{loc: 'UK'}])
 *
 */
Index.prototype.search = async function(query, filters, options = {}) {
  const defaults = {page: 0, limit: 100}
  options = {...defaults, ...options}
  const data = {
    query,
    rules: filters,
    ...options
  }
  const config = {
    method: 'post',
    url: `${this.searchPrefix}search`,
    headers: {
      'Accept': 'application/json'
    },
    data
  };
  const res = await axios(config)
  return res.data
}
module.exports = Index
