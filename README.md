# RapidAccess Search API client for JavaScript #

This API client allows you to intract with RapidAccess API endpoints.

### Getting Started ###
First, install RapidAccess JavaScript API Client via the npm package manager:
```bash
npm install @rapidaccess/js
```

Then, create objects on your index:

```js
const RapidAccess = require("@rapidaccess/js");

const client = RapidAccess.client("YourAPIKey");
const index = client.initIndex("yourIndexName");

const objectId = 'resourceId'
const object = {
  name: "Foo",
  description: "Bar"
}
const rules = [{
  country: ["AU", "US"]
}]

index
  .createObject(objectId, object, rules)
  .then(({ objectId }) => {
    console.log(objectID);
  })
  .catch(err => {
    console.log(err);
  });
```

Finally, let's actually search using the `search` method:

```js
index
  .search("Fo", [country: "AU"], {page: 0, limit: 100})
  .then(({ results }) => {
    console.log(results);
  })
  .catch(err => {
    console.log(err);
  });
```
