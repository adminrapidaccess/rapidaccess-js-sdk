require('dotenv').config()
const axios = require('axios');
const Client = require('./lib/Client');

const baseURL = 'https://rapidaccess.io'

axios.defaults.params = {}
/**
 * A proxy for [Client]{@link Client} to initalize package HTTP client
 *
 * @param {string} apiKey - client API access key
 * @param {string} [playground] - playground Url
 * @return {Client} client - a [client]{@link Client} object
 *
 * @example
 * const RapidAccess = require('@rapdaccess/js')
 * const client = RapidAccess.client("yourApiKey")
 *
 */
const client = function(apiKey, playgroundURL) {
  axios.defaults.baseURL = playgroundURL || baseURL
  axios.defaults.params['apiKey'] = apiKey
  return new Client(apiKey)
}

module.exports = {client}
